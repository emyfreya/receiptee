import 'package:json_annotation/json_annotation.dart';

part 'place_dto.g.dart';

@JsonSerializable(createToJson: false)
class PlaceDto {
  final String id;

  const PlaceDto({
    required this.id,
  });

  factory PlaceDto.fromJson(Map<String, dynamic> json) =>
      _$PlaceDtoFromJson(json);
}

@JsonSerializable(createToJson: false)
class PlaceListDto {
  final int total;
  final List<PlaceDto> items;

  const PlaceListDto({
    required this.total,
    required this.items,
  });

  factory PlaceListDto.fromJson(Map<String, dynamic> json) =>
      _$PlaceListDtoFromJson(json);
}
