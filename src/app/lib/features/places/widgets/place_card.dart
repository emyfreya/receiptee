import 'package:flutter/material.dart';

import '../models/place.dart';

class PlaceCard extends StatefulWidget {
  final Place place;

  const PlaceCard({required this.place, super.key});

  @override
  State<PlaceCard> createState() => _PlaceCardState();
}

class _PlaceCardState extends State<PlaceCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.place.name),
                  Text(widget.place.address.toString()),
                ],
              ),
            ),
            const Icon(Icons.arrow_right),
          ],
        ),
      ),
    );
  }
}
