import 'package:equatable/equatable.dart';

final class PlaceAddress extends Equatable {
  final String country;
  final String city;
  final String? zipCode;
  final String? addressLineTwo;
  final String? addressLineOne;

  const PlaceAddress(
      {required this.country,
      required this.city,
      this.zipCode,
      this.addressLineTwo,
      this.addressLineOne});

  @override
  List<Object?> get props =>
      [country, city, zipCode, addressLineTwo, addressLineOne];

  @override
  String toString() {
    return '$city, $country';
  }
}
