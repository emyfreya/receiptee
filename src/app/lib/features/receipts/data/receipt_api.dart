import 'receipt_dto.dart';

class ReceiptApi {
  Future<List<ReceiptTileDto>> getAll({required int page, required int limit}) {
    return Future.value([
      ReceiptTileDto(
        id: 3,
        totalNet: 0,
        totalGross: 0,
        totalTaxes: 0,
        lineCount: 0,
        createdAt: DateTime(2023, 1, 1),
        modifiedAt: DateTime(2023, 1, 1),
        storeLocation: "Here",
        storeName: "McDonald's",
      ),
      ReceiptTileDto(
        id: 4,
        totalNet: 0,
        totalGross: 0,
        totalTaxes: 0,
        lineCount: 0,
        createdAt: DateTime(2023, 1, 1),
        modifiedAt: DateTime(2023, 1, 1),
        storeLocation: "Over there",
        storeName: "Burger King",
      ),
    ]);
  }
}
