// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'receipt_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReceiptTileDto _$ReceiptTileDtoFromJson(Map<String, dynamic> json) =>
    ReceiptTileDto(
      id: json['id'] as int,
      totalNet: (json['totalNet'] as num).toDouble(),
      totalGross: (json['totalGross'] as num).toDouble(),
      totalTaxes: (json['totalTaxes'] as num).toDouble(),
      lineCount: json['lineCount'] as int,
      createdAt: DateTime.parse(json['createdAt'] as String),
      modifiedAt: DateTime.parse(json['modifiedAt'] as String),
      storeLocation: json['storeLocation'] as String,
      storeName: json['storeName'] as String,
    );
