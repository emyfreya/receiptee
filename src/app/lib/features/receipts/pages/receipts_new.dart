import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../components/bottom_navigation_bar.dart';
import '../../../routes.dart';
import '../models/receipts_model.dart';

class ReceiptsNewPage extends StatefulWidget {
  const ReceiptsNewPage({super.key});

  @override
  State<StatefulWidget> createState() => _ReceiptsNewPageState();
}

class _ReceiptsNewPageState extends State<ReceiptsNewPage> {
  final _dateinput = TextEditingController();
  final List<ReceiptLineModel> _receiptLines = [];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_dateinput.text.isEmpty) {
      DateTime now = DateTime.now();
      _dateinput.text =
          AppLocalizations.of(context)!.receiptDateFormat(now, now);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _dateinput.dispose();
  }

  void _addNewReceiptLine() {
    setState(() {
      _receiptLines.add(ReceiptLineModel(productName: '', price: 0.0));
    });
  }

  Widget _buildLine(ReceiptLineModel model) {
    return Container(
      color: _receiptLines.indexOf(model) % 2 == 0
          ? const Color(0x11C6C6C6)
          : const Color(0x22D6D6D6),
      margin: const EdgeInsets.symmetric(horizontal: 8),
      padding: const EdgeInsets.all(4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            flex: 2,
            child: TextFormField(
              decoration: const InputDecoration(
                hintText: 'Product name',
              ),
            ),
          ),
          const SizedBox(
            width: 8,
          ),
          Expanded(
            child: TextFormField(
              decoration: const InputDecoration(
                hintText: 'Price',
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _selectDate() async {
    DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime.now(),
      currentDate: DateTime.now(),
    );

    if (pickedDate == null) return;
    if (!mounted) return;

    TimeOfDay? time = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (time == null) return;

    DateTime finalDate =
        pickedDate.copyWith(hour: time.hour, minute: time.minute);

    setState(() {
      _dateinput.text =
          AppLocalizations.of(context)!.receiptDateFormat(finalDate, finalDate);
    });
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations locale = AppLocalizations.of(context)!;

    return Scaffold(
      bottomNavigationBar: const AppBottomNavigationBar(),
      appBar: AppBar(
        title: Text(locale.receiptsNewPageTitle),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamed(context, Routes.receipts),
        tooltip: 'Save',
        child: const Icon(Icons.save),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
        child: SingleChildScrollView(
          child: Column(
            children: [
              // Store choice
              TextFormField(
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Store',
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              // Receipt date
              TextFormField(
                controller: _dateinput,
                keyboardType: TextInputType.datetime,
                readOnly: true,
                onTap: () => _selectDate(),
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Date',
                  icon: Icon(Icons.calendar_today),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Please enter a date for your task";
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 8,
              ),
              Text('Receipt lines (${_receiptLines.length})'),
              TextButton(
                onPressed: () => _addNewReceiptLine(),
                child: const Text('Add new line'),
              ),
              Column(
                children: _receiptLines.map((e) => _buildLine(e)).toList(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
