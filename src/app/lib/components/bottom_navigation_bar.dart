import 'package:flutter/material.dart';

import '../app_localizations.dart';
import '../routes.dart';

class AppBottomNavigationBar extends StatefulWidget {
  const AppBottomNavigationBar({super.key});

  @override
  State<AppBottomNavigationBar> createState() => _AppBottomNavigationBarState();
}

class _AppBottomNavigationBarState extends State<AppBottomNavigationBar> {
  final List<_BarNavigationItemMetadata> _items = [
    _BarNavigationItemMetadata(
      icon: const Icon(Icons.home),
      label: (locale) => locale.home,
      route: Routes.home,
    ),
    _BarNavigationItemMetadata(
      icon: const Icon(Icons.receipt),
      label: (locale) => locale.receipt,
      route: Routes.receipts,
    ),
    _BarNavigationItemMetadata(
      icon: const Icon(Icons.store),
      label: (locale) => locale.place,
      route: Routes.places,
    ),
    _BarNavigationItemMetadata(
      icon: const Icon(Icons.manage_accounts),
      label: (locale) => locale.account,
      route: '/',
    ),
  ];

  void onTap(BuildContext context, int index) {
    Navigator.pushNamed(context, _items[index].route);
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations locale = AppLocalizations.of(context)!;

    return BottomNavigationBar(
      currentIndex: 0,
      onTap: (index) => onTap(context, index),
      type: BottomNavigationBarType.fixed,
      items: _items
          .map(
            (meta) => BottomNavigationBarItem(
              icon: meta.icon,
              label: meta.label(locale),
            ),
          )
          .toList(),
    );
  }
}

class _BarNavigationItemMetadata {
  final Icon icon;
  final String Function(AppLocalizations locale) label;
  final String route;

  _BarNavigationItemMetadata({
    required this.icon,
    required this.label,
    required this.route,
  });
}
