# README

A project to discover the BLoC architecture.

This project is to trace every receipt received, and out of the gathered data, display beautiful graphics representing what, where and how our receipts reflects our spending.

## Documentation

- [bloclibrary](https://bloclibrary.dev/#/coreconcepts?id=cubit-vs-bloc)
- [Flutter Infinite List Tutorial](https://bloclibrary.dev/#/flutterinfinitelisttutorial)