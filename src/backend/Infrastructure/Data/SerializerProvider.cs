﻿namespace Receiptee.Infrastructure.Data;

internal sealed class SerializerProvider : IBsonSerializationProvider
{
    public IBsonSerializer? GetSerializer(Type type)
    {
        if (type.IsAssignableTo(typeof(StoreId))) return new StoreIdSerializer();

        return null;
    }
}
