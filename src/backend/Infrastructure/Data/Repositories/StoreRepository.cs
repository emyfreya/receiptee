﻿namespace Receiptee.Infrastructure.Data.Repositories;

internal sealed class StoreRepository : IStoreRepository
{
    private readonly IMongoCollection<StoreDb> _stores;

    public StoreRepository(IMongoDatabase db)
    {
        _stores = db.GetCollection<StoreDb>("store");
    }

    public Task<StoreDb> GetAsync(StoreId storeId, CancellationToken cancellationToken = default)
    {
        return _stores
            .Aggregate()
            .Match(e => e.Id == storeId)
            .SingleAsync(cancellationToken);
    }

    public async ValueTask<OffsetCollection<StoreDb>> GetAllAsync(int offset, int limit, CancellationToken cancellationToken = default)
    {
        IFindFluent<StoreDb, StoreDb> query = _stores.Find(FilterDefinition<StoreDb>.Empty)
            .Skip(offset)
            .Limit(limit);

        long total = await query.CountDocumentsAsync(cancellationToken);
        List<StoreDb> items = await query.ToListAsync(cancellationToken);

        return new OffsetCollection<StoreDb>(items, offset, total);
    }

    public Task InsertAsync(StoreDb db, CancellationToken cancellationToken = default)
    {
        return _stores.InsertOneAsync(db, options: null, cancellationToken);
    }
}
