﻿namespace Receiptee.Infrastructure.Data.Entities;

public sealed class ProductDb
{
    public ICollection<ReceiptLineDb> ReceiptLines { get; set; }

    public int Id { get; set; }

    public string Label { get; set; } = null!;

    /// <summary>
    /// Without VAT.
    /// </summary>
    public decimal Price { get; set; }

    public string VatCode { get; set; } = null!;

    public VatDb Vat { get; set; } = null!;

    public int CategoryId { get; set; }

    public ProductCategoryDb? Category { get; set; }

    public ProductDb()
    {
        ReceiptLines = new HashSet<ReceiptLineDb>();
    }
}
