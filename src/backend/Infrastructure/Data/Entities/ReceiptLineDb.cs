﻿namespace Receiptee.Infrastructure.Data.Entities;

public sealed record ReceiptLineDb
{
    /// <summary>
    /// PrimaryKey part.
    /// </summary>
    public ushort RowNumber { get; set; }

    public string Label { get; set; } = null!;

    public int Quantity { get; set; }

    public decimal PriceNet { get; set; }

    public decimal PriceGross { get; set; }

    public VatDb Vat { get; set; } = null!;

    public string VatCode { get; set; } = null!;

    public ReceiptDb Receipt { get; set; } = null!;

    /// <summary>
    /// PrimaryKey part.
    /// </summary>
    public int ReceiptId { get; set; }

    public ProductDb Product { get; set; } = null!;

    public int ProductId { get; set; }
}
