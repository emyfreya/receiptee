﻿using Receiptee.Features.Payments;

namespace Receiptee.Infrastructure.Data.Entities;

public sealed record PaymentDb
{
    public ICollection<ReceiptPaymentDb> ReceiptPayments { get; set; }

    public PaymentKind Kind { get; set; }

    public string Label { get; set; } = null!;

    public PaymentDb()
    {
        ReceiptPayments = new HashSet<ReceiptPaymentDb>();
    }
}
