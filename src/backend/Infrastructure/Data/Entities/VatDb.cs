﻿namespace Receiptee.Infrastructure.Data.Entities;

public sealed record VatDb
{
    public ICollection<ProductDb> Products { get; set; }

    public ICollection<ReceiptLineDb> ReceiptLines { get; set; }

    public string Code { get; set; } = null!;

    public decimal Value { get; set; }

    public VatDb()
    {
        Products = new HashSet<ProductDb>();
        ReceiptLines = new HashSet<ReceiptLineDb>();
    }
}
