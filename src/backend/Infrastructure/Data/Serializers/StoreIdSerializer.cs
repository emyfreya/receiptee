﻿namespace Receiptee.Infrastructure.Data.Serializers;

public sealed class StoreIdSerializer : SerializerBase<StoreId>
{
    public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, StoreId value)
    {
        context.Writer.WriteString(value.Value.ToString());
    }

    public override StoreId Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
    {
        string guid = context.Reader.ReadString();
        return new StoreId(new Guid(guid));
    }
}