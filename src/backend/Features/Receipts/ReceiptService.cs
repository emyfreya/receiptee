﻿using Receiptee.Features.Payments;

namespace Receiptee.Features.Receipts;

internal sealed class ReceiptService
{
    private readonly ReceiptRepository _receiptRepository;
    private readonly StoreRepository _storeRepository;

    public ReceiptService(ReceiptRepository receiptRepository, StoreRepository storeRepository)
    {
        _receiptRepository = receiptRepository;
        _storeRepository = storeRepository;
    }

    public async ValueTask AddReceiptAsync(Receipt receipt, CancellationToken cancellationToken = default)
    {
        StoreDb storeDb = await _storeRepository.GetAsync(receipt.StoreId, cancellationToken);

        ReceiptDb receiptDb = new()
        {
            CreatedAt = DateTime.UtcNow,
            PaidAt = receipt.PaidAt,
            Store = storeDb
        };

        foreach (PaymentKind paymentKind in receipt.Payments)
        {
            receiptDb.Payments.Add(new ReceiptPaymentDb
            {
                PaymentKind = paymentKind,
                Payment = new PaymentDb
                {
                    Kind = paymentKind
                }
            });
        }

        foreach (ReceiptLine receiptLine in receipt.Lines)
        {
            ReceiptLineDb receiptLineDb = new()
            {
                VatCode = receiptLine.VatCode,
                Label = receiptLine.Label,
                PriceNet = receiptLine.PriceNet,
                PriceGross = receiptLine.PriceGross,
                Quantity = receiptLine.Quantity,
                RowNumber = receiptLine.RowNumber,
                ProductId = receiptLine.Product.Id
            };

            receiptDb.Lines.Add(receiptLineDb);
        }

        //await _receiptRepository.AddAsync(receiptDb, cancellationToken);
    }
}
