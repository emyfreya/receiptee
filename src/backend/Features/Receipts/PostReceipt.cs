﻿using Receiptee.Receipts;

namespace Receiptee.Features.Receipts;

public class PostReceipt : Endpoint<CreateReceiptRequest, CreateReceiptResponse>
{
    public override void Configure()
    {
        Post("receipts");
    }

    public override async Task HandleAsync(CreateReceiptRequest req, CancellationToken ct)
    {
        await SendResultAsync(Results.Ok(new CreateReceiptResponse
        {
            Id = new ReceiptId(Guid.NewGuid()),
            Receipt = req
        }));
    }
}
