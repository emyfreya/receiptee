﻿namespace Receiptee.Features.Receipts;

[JsonConverter(typeof(JsonStringEnumConverter<ReceiptType>))]
public enum ReceiptType
{
    Store,
    GasStation,
    Card
}
