﻿using Receiptee.Features.Payments;

namespace Receiptee.Features.Receipts;

public sealed record Receipt
{
    public required DateTime PaidAt { get; init; }

    public required StoreId StoreId { get; init; }

    public required ICollection<PaymentKind> Payments { get; init; }

    public required ICollection<ReceiptLine> Lines { get; init; }

    public required ReceiptId Id { get; init; }
}
