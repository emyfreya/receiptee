﻿namespace Receiptee.Receipts;

public record CreateReceiptResponse
{
    public required ReceiptId Id { get; init; }

    public required CreateReceiptRequest Receipt { get; init; }
}
