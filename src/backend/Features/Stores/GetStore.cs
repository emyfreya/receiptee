﻿namespace Receiptee.Features.Stores;

public sealed class GetStore : Endpoint<GetStoreRequest, OffsetCollection<Store>, StoreMapper>
{
    private readonly IStoreRepository _storeRepository;

    public GetStore(IStoreRepository storeRepository)
    {
        _storeRepository = storeRepository;
    }

    public override void Configure()
    {
        Get("stores");

        SerializerContext(StoreSerializationContext.Default);
    }

    public override async Task HandleAsync(GetStoreRequest rq, CancellationToken ct)
    {
        OffsetCollection<StoreDb> storeDbs = await _storeRepository.GetAllAsync(rq.Offset, rq.Limit, ct);

        OffsetCollection<Store> stores = new(storeDbs.Items.Select(e => Map.FromEntity(e)).ToArray(), storeDbs.Offset, storeDbs.Total);

        await SendOkAsync(stores, ct);
    }
}

public sealed record GetStoreRequest([FromQuery] int Offset = 0, [FromQuery] int Limit = 10);
