# README

## Principle

The app is multi plateform : Windows, Web, Android, iOS.

The app allows to manage receipts such as adding, removing, sorting, searching through.

The API is the backend. Stores all the data needed, per user.

## Projects

- Frontend, powered by Flutter.
- Backend, powered by C#, PostgreSQL, MongoDB.
- Ocr project, made in rust.